#!/bin/bash
#pseudo script used to build package
APPNAME="fr.videosub.revocalcrms"
export JAVA_HOME=/usr
export ANDROID_SDK_ROOT=/opt/android/sdk/
unset ANDROID_HOME
export PATH=${PATH}:$ANDROID_SDK_ROOT:$ANDROID_SDK_ROOT/tools:$ANDROID_SDK_ROOT/platform-tools:$ANDROID_SDK_ROOT/tools/bin/
SIGNID="ryxeo"
SIGFILE=~/bin/android_release.keystore
STOREPASS=`cat ~/.android-key_pass`

fullInstall=""

#par defaut on n'essaye pas de déployer via ADB
adbInstall="N"

APPVERSION=`grep ${APPNAME} config.xml | sed s/".*version=\""/""/ | cut -d '"' -f1`

function usage {
    echo "Usage: $(basename $0) [-n] [-f]" 2>&1
    echo "  -n : build leger"
    echo "  -f : build complet avec suppression et réinstallation des plugins & base"
    echo "  -i : déploiement sur le téléphone via ADB à la fin du build"
    exit 1
}

optstring=":nfhi"
while getopts ${optstring} arg; do
    case "${arg}" in
        h)
            usage
        ;;
        n)
            fullInstall="N"
        ;;
        f)
            fullInstall="o"
        ;;
        i)
            echo "- on fait une installation via ADB à la fin ..."
            adbInstall="o"
        ;;
        *)
            usage
        ;;
    esac
done

if [[ ${#} -eq 0 ]]; then
    echo "Voulez vous faire un build complet avec suppression et réinstallation des plugins & base ?"
    echo -n "[o/N]"
    read fullInstall
fi

#dans tous les cas on fait place nette sinon par exemple le versionCode n'est pas mis à jour
rm -rf platforms/android/app/build/*

if [ "${fullInstall}" == "o" ]; then
    cordova platform remove android
    rm -rf plugins
    rm -rf platforms/android
    cordova platform add android
fi

cordova build android --release
if [ ${?} -eq "0" ]; then
    echo "build ok ... la suite"
    if [ -z "${STOREPASS}" ]; then
        echo "Il manque la passphrase pour utiliser la clé de signature de l'app..."
        exit -2
    fi

    OUTFILE="./platforms/android/app/build/outputs/bundle/release/app-release.aab"
    OUTAPK="./platforms/android/app/build/outputs/apk/release/app-release.apks"
    # jarsigner -verbose -sigalg SHA256withRSA -digestalg SHA-256 -keystore ${SIGFILE} -storepass ${STOREPASS} ${OUTFILE}  ${SIGNID}
    jarsigner -verbose -sigalg SHA256withRSA -digestalg SHA-256 -keystore ${SIGFILE} -storepass ${STOREPASS} ${OUTFILE}  ${SIGNID}

    if [ ${?} -eq "0" ]; then
        echo "sign ok ... la suite"
        java -jar /opt/android/bundletool-all-1.8.0.jar build-apks --bundle=${OUTFILE} \
        --output=${OUTAPK} --ks=${SIGFILE} --ks-key-alias=${SIGNID} --ks-pass=pass:${STOREPASS}

        #copy file to /tmp
        TMPFILENAME="/tmp/${APPNAME}-${APPVERSION}.aab"
        echo "Copie du paquet vers ${TMPFILENAME}"
        cp ${OUTFILE} ${TMPFILENAME}

        if [ "${adbInstall}" == "o" ]; then
            echo -n "Installation en cours via bundletool ..."
            #jarsigner -verbose -sigalg SHA256withRSA -digestalg SHA-256 -keystore ${SIGFILE} -storepass ${STOREPASS} ${OUTAPK}  ${SIGNID}
            #adb install ${OUTAPK}
            java -jar /opt/android/bundletool-all-1.8.0.jar install-apks --apks=${OUTAPK}
        else
            echo "Si vous voulez installer manuellement le paquet :"
            echo "java -jar /opt/android/bundletool-all-1.8.0.jar install-apks --apks=${OUTAPK}"
        fi

    else
        echo "erreur de signature"
        exit -2
    fi
else
    echo "erreur"
    exit -1
fi

